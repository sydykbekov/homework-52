import React from 'react'

const NewLotoNumbers = ({numbers}) => (
    <div id="container">
        {numbers.map((number, i) => <div className="loto" key={i}>{number}</div>)}
    </div>
);

export default NewLotoNumbers;
