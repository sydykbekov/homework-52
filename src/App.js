import React, {Component} from 'react';
import './App.css';
import NewLotoNumbers from './NewLotoNumbers'

class App extends Component {
    state = {
        numbers: []
    };

    generate() {
        let array = [];
        while (array.length < 5) {
            let newWord;
            let randomNum = Math.floor(Math.random() * 28) + 5;
            array.forEach(function(item) {
                if (item === randomNum) {
                    newWord = 'identical';
                }
            });
            if (newWord !== 'identical') {
                array.push(randomNum);
            }
        }
        function compareNumbers(a, b) {
            return a - b;
        }
        array.sort(compareNumbers);
        this.setState({numbers: array})
    }

    componentDidMount() {
        this.generate()
    }

    handleClick = () => {
        this.generate();
        document.getElementById('coin').style.display = 'block';
    };

    render () {
        return (
            <div className="App">
                <button id="generate" onClick={this.handleClick}>Generate</button>
                <NewLotoNumbers numbers={this.state.numbers} />
            </div>
        );
    }
}

export default App;
